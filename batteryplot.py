"""Script pour permettant de créer un courbe de la tension de la batterie en fonction du temps"""

from time import sleep, time

from handypower.battery import Battery

bat = Battery(0, 1200, 150)
start = time()
MIN_VOLT = 10

log = open("./battery_log.csv", "x")
log.write("time;voltage;\n")
while bat.voltage > MIN_VOLT:
    sleep(5)
    data = "{time};{voltage};\n".format(
        time=time() - start, voltage=bat.voltage)
    print(data)
    log.write(data)
