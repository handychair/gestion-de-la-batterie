"""Module permettant d'obtenir des informations sur la batterie du HandyChair"""
from math import pow

from grove import adc


class Battery:
    """Classe permettant d'obtenir des informations sur la batterie du handychair"""

    def __init__(self,
                 pin_adc,
                 resistor_1,
                 resistor_2,
                 discharge_current=7.9,
                 capacity=84):
        """Executée à la création d'une instance de la classe
        Paramètres:
            - pin_adc : nom de l'entrée analogique sur laquelle est connectée la batterie
            - resistor_1 : valeur en Ohm de la resistance 1 ( pt diviseur de tension )
            - resistor_2 : valeur en Ohm de la resistance 2 ( pt diviseur de tension )
        """
        self.pin_adc = pin_adc
        self.resistor_1 = resistor_1
        self.resistor_2 = resistor_2
        self.adc = adc.ADC()
        self.discharge_current = discharge_current
        self.capacity = capacity

    @property
    def voltage(self):
        """Retourne la tension de la batterie"""
        return (self.adc.read_voltage(self.pin_adc) / 1000) / (
            self.resistor_2 / (self.resistor_1 + self.resistor_2))

    @property
    def percentage(self):
        """Retourne la charge restante de batterie en pourcentage"""
        t = self.voltage / 2
        return 21.7900715993961 * pow(t, 4) - 1006.3910688498 * pow(
            t, 3) + 17442.914626039 * pow(
                t, 2) - 134412.167386785 * t + 388420.076412133

    @property
    def battery_life(self):
        return (self.capacity * self.percentage/100) / self.discharge_current
