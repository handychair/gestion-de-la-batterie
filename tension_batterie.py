#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Script de test pour afficher la valeur de la tension récupérée par le CAN a travers le pont diviseur de tension"""
from time import sleep

from handypower.battery import Battery

bat = Battery(0, 1000, 150)

while True:
    print("Tension : {}\r".format(bat.voltage), end="")
    sleep(.5)
