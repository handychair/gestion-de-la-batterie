#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Programme principal de gestion de la batterie
"""
import time

import handypower.battery as hb
from rpi_ws281x import Adafruit_NeoPixel

LED_COUNT = 10  # Number of LED pixels.
LED_PIN = 18  # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10  # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
# True to invert the signal (when using NPN transistor level shift)
LED_INVERT = False

PIN_BATTERY = 0
LED_BLINK_DURATION = 1

battery = hb.Battery(PIN_BATTERY, 1000, 150)
strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT,
                          LED_BRIGHTNESS)
strip.begin()
last_blink = 0
leds_status = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0],
               [0, 0], [0, 0]]
# leds_status : liste de l'état des leds
# dans chaque tupple : la première valeur : si cette led "doit" etre allumée
# la seconde, si elle est allumée
# l'usage de deux valeur permet de faire clignoter uniquement les bonnes leds
while True:
    percentage = battery.percentage
    battery_life = battery.battery_life

    nb_led = round(percentage / 10)
    if nb_led > 10 : 
        nb_led = 10
   
    if 0 < percentage < 104:
        i = 0
        for i in range(0, nb_led):
            leds_status[i][0] = True

        for i in range(i + 1, 10):
            leds_status[i][0] = False

        if battery_life < 0.5 and (
                time.time() - last_blink) >= LED_BLINK_DURATION:
            state = not leds_status[0][1]
            for index, value in enumerate(leds_status):
                if leds_status[index][0]:
                    leds_status[index][1] = state
                else:
                    leds_status[index][1] = False
            last_blink = time.time()
        elif battery_life >= 0.5:
            for index, value in enumerate(leds_status):
                if leds_status[index][0]:
                    leds_status[index][1] = True
                else:
                    leds_status[index][1] = False

    str = "{} ".format(round(percentage))
    for index, value in enumerate(leds_status):
        if value[1]:
            str += "0"
            strip.setPixelColorRGB(index, 255, 255, 255)
        else:
            str += "-"
            strip.setPixelColorRGB(index, 0, 0, 0)
    str += " | Time Left(h): {}".format(battery_life)
    strip.show()
    print(str, end="\r")
    time.sleep(.2)
